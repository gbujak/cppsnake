#include "../include/snake.hpp"

bool Snake::has_point(const Point& point) const {
    for (const Point& i : body) {
        if (point.x == i.x && point.y == i.y)
            return true;
    }
    return false;
}

void Snake::move(Apple& apple) {
    Point a_pos = apple.get_pos();
    Point npoint = *body.begin();
    npoint.x += dx;
    npoint.y += dy;
    body.push_front(npoint);
    if (a_pos.x != npoint.x || a_pos.y != npoint.y)
        body.pop_back();
    else apple.eat();
}