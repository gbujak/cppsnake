#include "../include/apple.hpp"
#include "../include/snake.hpp"

Apple::Apple(const Snake& snake,  const Game_Map& map) {
    auto dimensions = map.get_size();
    do {
        pos = {
            (char) ((rand() % dimensions.first ) - dimensions.first / 2 ),
            (char) ((rand() % dimensions.second) - dimensions.second / 2)
        };
    } while (snake.has_point(pos) == true);
}