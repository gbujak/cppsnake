#pragma once

#include "apple.hpp"
#include "snake.hpp"
#include "game_map.hpp"

struct Point {
    char x, y;
    Point(char x, char y)
        : x(x), y(y) {}
    Point() = default;
};