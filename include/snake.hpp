#pragma once

#include <list>

#include "apple.hpp"
#include "game_map.hpp"
#include "point.hpp"

class Apple;

class Snake {
    std::list<Point> body;
    char dx, dy; // Velocity
    public:
    Snake() {
        for (char i = 0; i < 5; i++)
            body.push_back(Point(0, i));
    }
    void move(Apple&);
    bool has_point(const Point&) const;
};