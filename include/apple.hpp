#pragma once

#include "snake.hpp"
#include "game_map.hpp"
#include "point.hpp"

class Snake;

class Apple {
    Point pos;
    bool eaten;
    public:
    Apple(const Snake& snake,  const Game_Map& map);
    Point get_pos() const { return pos; }
    void eat() { eaten = true; }
    bool is_eaten() const { return eaten; }
};