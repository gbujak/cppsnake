#pragma once

#include <iostream>

#include "apple.hpp"
#include "snake.hpp"
#include "point.hpp"

class Game_Map {
    unsigned char width, height;
    public:
    Game_Map(unsigned char w, unsigned char h)
        : width(w), height(h) {
        if (width % 2 == 0 || height % 2 == 0) {
            std::cout << "Game_Map dimensions must be odd" << std::endl;
            abort();
        }
    }
    bool has_point(const Point& p) const {
        if (abs(p.x) <= width / 2 && abs(p.y) <= height / 2)
             return true;
        else return false; 
    }
    std::pair<int, int> get_size() const {
        return std::make_pair(width, height);
    }
};