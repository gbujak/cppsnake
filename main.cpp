#include <list>
#include <iostream>
#include <tuple>
#include <SDL2/SDL.h>

#include "include/snake.hpp"
#include "include/apple.hpp"
#include "include/point.hpp"
#include "include/game_map.hpp"

#define Window_Height 600
#define Window_Width 800

int main(void) {
    srand(time(NULL));
    if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
        std::cout << SDL_GetError() << std::endl;
    
    SDL_Window* window = SDL_CreateWindow(
        "Snake", 100, 100,
        Window_Width, Window_Height, SDL_WINDOW_SHOWN
    );
    if (window == nullptr)
        std::cout << "window error: " << SDL_GetError() << std::endl;

    SDL_Renderer* renderer = SDL_CreateRenderer(
        window, -1, SDL_RENDERER_ACCELERATED
    );
    if (renderer == nullptr)
        std::cout << "renderer error: " << SDL_GetError() << std::endl;

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderSetLogicalSize(renderer, Window_Width, Window_Height);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);

    SDL_Delay(4000);



    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
}